#!/usr/bin/env python3

import unittest
from sequence import SequenceBase

class TestSequence(unittest.TestCase):

    def test_init01(self):
        # Comprueba que el constructor guarda los datos suministrados
        seq = SequenceBase("ATGC")
        self.assertEqual(seq.seq, "ATGC")



if __name__ == "__main__":
    unittest.main()
